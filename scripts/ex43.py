from sys import exit
from random import randint
from textwrap import dedent

class Scene(object):
    def enter(self):
        print("This scene is not yet configured.")
        print("Subclass it and implement enter()")
        exit(1)

class Engine(object):
    def __init__(self, scene_map):
        self.scene_map = scene_map
    def play(self):
        current_scene = self.scene_map.opening_scene()
        last_scene = self.scene_map.next_scene('finished')

        while current_scene !=last_scene:
            next_scene_name = current_scene.enter()
            current_scene = self.scene_map.next_scene(next_scene_name)

class Death(Scene):
    quips = [
        "You died. You kinda suck at this.",
        "Your mon would be pround...if she were smarter.",
        "Such a luser.",
        "I have a small puppy that's better at this.",
        "You're worse than your Dad's jokes."
    ]
    def enter(self):
        print(Death.quips[randint(0, len(self.quips)-1)])
        exit(1)

class CentralCorridor(Scene):
    def enter(self):
        print(dedent("""
            The Kaju have invaded your ship and destroyed your entire crew.
            """))
        action = input(">> ")

        if action == "shoot!":
            print(dedent("""
            The Kaju eats you.
            """))
            return 'death'
        elif action == "dodge!":
            print(dedent("""
                You wakup shortly after only to die as the Gothon stomps on your head and eats you.
                """))
            return 'death'
        elif action == "tell a joke":
            print(dedent("""
                Gothon stops, tries not to laugh,then busts out laughing and can't move.
                """))
            return 'laser_weapon_armory'
        else:
            print("DOES NOT COMPUTE!")
            return 'central_corridor'        

class LaserWeaponArmory(Scene):
    def enter(self):
        print(dedent("""
            There's a keypad lock on the box and you need the code to get the bomb out.
            If you get the code wrong 10 times then the lock closes forever and you can't
            get the bomb. The code is 3 digits. 
            """))
        code = f"{randint(1,9)} {randint(1,9)} {randint(1,9)}"
        guess = input("[keypad]>> ")
        guesses = 0
        print(f"{code}")

        while guess != code and guesses < 10:
            print("BzzzeDDD!")
            guesses += 1
            guess = input("[keypad]>> ")

        if guess == code:
            print(dedent("""
                  You grab the neutron bomb and run as fast as you can to the bridge.
                  """))
            return 'the_bridge'
        else:
            print(dedent("""
                         The lock buzzes one last time and Gothons blow up the ship and you die.
                         """))
            return 'death'
        
class TheBridge(Scene):
    def enter(self):
        print(dedent("""
                    You brust onto the bridge.And There are 5 Gothons who are trying
                     to kill you.
                     """))
        action = input(">> ")
        if action == "throw the bomb":
            print(dedent("""
                        You die konwing they will probably blow up when ti goes off.
                         """))  
            return 'death'
        elif action == "slowly place the bomb":
            print(dedent("""
                    Now that the bomb is placed you run to the escape pod to get off this tin can.
                    """))
            return 'escape_pod'
        else:
            print("DOES NOT COMPUTE!")
            return "the_bridge"
             
class EscapePod(Scene):
    def enter(self):
        print(dedent("""
                There's 5 pods, which one do you take?
                """))

        good_pod = randint(1,5)
        print(f"{good_pod}")
        guess = input("[pod #]> ")

        if int(guess) != good_pod:
            print(dedent("""
                    You jump into pod {guess}. The pod escapes into the void of space.You die.
                    """))
            return 'death'
        else:
            print(dedent("""
                        You win!
                         """))
        return 'finished'

class Finished(Scene):
    def enter(self):
        print("You won! Good Job!")
        return 'finished'

class Map(object):
    scenes = {
        'central_corridor': CentralCorridor(),
        'laser_weapon_armory': LaserWeaponArmory(),
        'the_bridge': TheBridge(),
        'escape_pod': EscapePod(),
        'death': Death(),
        'finished': Finished()
    }
    def __init__(self, start_scene):
        self.start_scene = start_scene

    def next_scene(self, scene_name):
        va1 = Map.scenes.get(scene_name)
        return va1
    
    def opening_scene(self):
        return self.next_scene(self.start_scene)

a_map = Map('central_corridor')
a_game = Engine(a_map)
a_game.play()