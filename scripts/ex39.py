# create a mapping of state to abbreviation
states = {
    'Oregon': 'OR',
    'Florida': 'FL',
    'California': 'CA',
    'New York': 'NY',
    'Michigan': 'MI'
}

# create a basic set of states and some cities in them
cities = {
    'CA': 'San Francisco',
    'MI': 'Detroit',
    'FL': 'Jacksonvile'
}

# add some more cities
cities['NY'] = 'New York'
cities['OR'] = 'PortLand'

# print out some cities
print('-' * 10)
print("NY State hae:", cities['NY'])
print("OR State has: ",cities['OR'])

# print out some states
print('-' * 10)
print("Michigan's abbreviation is: ", states['Michigan'])
print("Florida's abbreviation is: ", states['Florida'])

# do it by using the state then cities dict
print('-' * 10)
print("Michigan has: ", cities[states['Michigan']])
print("Florida has: ", cities[states['Florida']])

# print every state abbreviation
print("-" * 10)
for state, abbrev in list(cities.items()):
    print(f"{state} is abbreviated {abbrev}")

# print every cities in state
print("-" * 10)
for abbrev, city in list(cities.items()):
    print(f"{abbrev} has city {city}")

# now do both at the same time
print("-" * 10)
for state, abbrev in list(states.items()):
    print(f"{state} state is abbreviated {abbrev}")
    print(f"and has city {cities[abbrev]}")

print("-" * 10)
# safely get a abbreviation by state that might not be there
state = states.get("Texas")

if not state:
    print("Sorry,no Texas")
    states['Texas'] = 'TX'

# get a city with a default value
city = cities.get('TX', 'Does not Exist.')
print(f"The city for the state 'TX' is: {city}")
cities['TX'] = 'Austin'

print(list(cities.items()))
print(list(states.items()))

print("-" * 10)
for state, abbrev in list(states.items()):
    print(f"{state} state is abbreviated {abbrev}")
    print(f"and has city {cities[abbrev]}")
